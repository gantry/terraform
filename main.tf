
# us-east-1 = Virginia
# us-east-2 = Ohio

provider "aws" {
  region = "us-east-2"
}

# ami = "ami-2d39803a"
resource "aws_instance" "gantry" {
  count = 7
  ami = "ami-f11a2894"
  instance_type = "t2.micro"
  vpc_security_group_ids = ["${aws_security_group.instance.id}"]  
  key_name = "engility-amazon"
  user_data = <<-EOF
              #!/bin/bash
              apt-get update && apt-get install -y ssh python tree htop iftop git unzip
              #echo "Hello, World" > index.html
              #nohup busybox httpd -f -p 8080 &
              EOF

  tags {
    Name = "terraform-gantry-${count.index}"
  }
}

resource "aws_instance" "dashboard" {
  ami = "ami-f11a2894"
  instance_type = "t2.micro"
  vpc_security_group_ids = ["${aws_security_group.instance.id}"]
  key_name = "engility-amazon"
  user_data = <<-EOF
              #!/bin/bash
              apt-get update && apt-get install -y ssh python tree htop iftop git unzip
              #echo "Hello, World" > index.html
              #nohup busybox httpd -f -p 8080 &
              EOF

  tags {
    Name = "terraform-dashboard"
  }
}

resource "aws_security_group" "instance" {
  name = "terraform-test-instance"

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

