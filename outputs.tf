

output "results" {
  value = "${
    jsonencode(
      map(
      "names", "${aws_instance.gantry.*.tags.Name}",
      "instance-ips", "${aws_instance.gantry.*.public_ip}",
      "instance_state", "${aws_instance.gantry.*.instance_state}",
      "key_name", "${aws_instance.gantry.*.key_name}",
      "tenancy", "${aws_instance.gantry.*.tenancy}"
        )
    )
  }"
}

//output "results2" {
//  value = [
//    "${jsonencode("name")}: ${jsonencode(aws_instance.gantry.*.tags.Name)}",
//    "${jsonencode("instance_ips")}: ${jsonencode(aws_instance.gantry.*.public_ip)}",
//    "${jsonencode("instance_state")}: ${jsonencode(aws_instance.gantry.*.instance_state)}",
//    "${jsonencode("key_name")}: ${jsonencode(aws_instance.gantry.*.key_name)}",
//    "${jsonencode("tenancy")}: ${jsonencode(aws_instance.gantry.*.tenancy)}",
//  ]
//}
//
//
//output "results3" {
//  value = "${
//    jsonencode(
//      map(
//      "names", "${jsonencode(aws_instance.gantry.*.tags.Name)}",
//      "instance-ips", "${jsonencode(aws_instance.gantry.*.public_ip)}"
//        )
//    )
//  }"
//}
//


//output "instance_ips" {
//  value = [
//    "${aws_instance.gantry.*.public_ip}",
//  ]
//}
//output "instance_state" {
//  value = ["${aws_instance.gantry.*.instance_state}"]
//}
//output "instance_type" {
//  value = ["${aws_instance.gantry.*.instance_type}"]
//}
//output "key_name" {
//  value = ["${aws_instance.gantry.*.key_name}"]
//}
//output "tenancy" {
//  value = ["${aws_instance.gantry.*.tenancy}"]
//}
//

