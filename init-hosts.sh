#!/usr/bin/env bash
CONTAINER_NAME=$1

#lxc launch ubuntu:14.04 $CONTAINER_NAME

echo "Setting up : $CONTAINER_NAME "

# Give Container time to spin-up
sleep 5

echo "Updating Hosts"

# Remove existing Container from hosts file (if exists)
sed -i "/$CONTAINER_NAME/d" hosts

# Grep "lxc info <container>" & then Awk IP Address from output
# https://www.brianparsons.net/FindIPAddresseswithawk/
# HOST_IP=$(lxc info $CONTAINER_NAME | grep eth0 | grep inet | grep -v inet6 | awk '{match($0,/[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/); ip = substr($0,RSTART,RLENGTH); print ip}')

TERRAFORM_NAMES=bash ./terraform-output.sh | grep names
TERRAFORM_IPS=bash ./terraform-output.sh | grep instance-ips
TERRAFORM_KEYS=bash ./terraform-output.sh | grep key_name


while [[ $# > 1 ]]; do

# Construct hosts file entry
# test0 ansible_ssh_host=10.64.254.102 ansible_ssh_user=ubuntu ansible_ssh_private_key_file=keys/engility.pem
HOSTS_ENTRY="$CONTAINER_NAME ansible_ssh_host=$HOST_IP ansible_ssh_user=ubuntu ansible_ssh_private_key_file=keys/engility.pem"

echo "New Hosts Entry"
echo $HOSTS_ENTRY
echo $HOSTS_ENTRY >> hosts

# Add container name & ip address to nginx
sudo sed -i -e 's/$CONTAINER_NAME.local/$HOST_IP/g' /etc/nginx/sites-available/default

done



